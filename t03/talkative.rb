module Talkative
  def shout(text)
    self.output text.upcase
  end

  def whisper(text)
    self.output text.downcase
  end

  def encrypt(text)
    self.output text.downcase.chars.map { |c| (c.ord + 3 <= 'z'.ord ? c.ord + 3 : c.ord + 3 - 'z'.ord + 'a'.ord - 1).chr }.join
  end
end