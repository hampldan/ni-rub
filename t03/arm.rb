# frozen_string_literal: true

# Arm
class Arm
  attr_reader :length
  attr_reader :type

  def initialize(length, type)
    raise 'ZERO' if length <= 0

    @length = length
    @type = type
  end

  def score
    {
      :poker => 1,
      :slasher => 3,
      :grabber => 5,
    }[@type]
  end
end
