# frozen_string_literal: true

require_relative 'validations.rb'
require_relative 'talkative.rb'

# Robot
class Robot
  attr_reader :name

  include Comparable
  include Talkative
  prepend Validations

  def <=>(other)
    score <=> other.score
  end

  def initialize(name)
    @name = name
    @arms = []
  end

  def score
    return 0 if @arms.length.zero?

    self.score_sum + self.avg_len
  end

  def score_sum
    return 0 if @arms.length.zero?

    @arms.map(&:score).sum
  end

  def avg_len
    return 0 if @arms.length.zero?

    @arms.map(&:length).sum / @arms.length
  end

  def add_arms(*value)
    @arms += value
  end

  def output(text)
    puts text
  end

  def introduce
    self.output "Jsem #{@name}."

    if @arms.length.zero?
      self.output "... a nemám ruce..."
    else
      self.output "A mám ruce:"
      handykek = @arms.map { |i| i.type }.tally
      handykek.each do |key, value|
        self.output "#{key} mám #{value}"
      end
    end
  end
end
