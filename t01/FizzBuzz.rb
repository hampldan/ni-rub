# frozen_string_literal: true

# no warnings:
# puts (1..100).map { |i| "#{'Fizz' if i % 3 < 1}#{'Buzz' if i % 5 < 1}#{i if i % 3 * i % 5 >= 1}" }.join(', ')
# puts (1..100).map { |i| i % 3 * i % 5 < 1 ? 'FizzBuzz'[(4 if i % 3 >= 1)..(3 if i % 5 >= 1)] : i }.join(', ')
# puts (1..100).map { |i| i % 3 * i % 5 < 1 ? "#{'Fizz' if i % 3 < 1}#{'Buzz' if i % 5 < 1}" : i }.join(', ')
# 1.upto(100) { |i| puts i % 3 * i % 5 < 1 ? "#{'Fizz' if i % 3 < 1}#{'Buzz' if i % 5 < 1}" : i }
# 1.upto(100) { |i| puts(s = "#{'Fizz' if i % 3 < 1}#{'Buzz' if i % 5 < 1}") == '' ? i : s }
1.upto(100) { |i| puts(s = "#{' Fizz' if i % 3 < 1}#{' Buzz' if i % 5 < 1}") == '' ? i : s.strip }
