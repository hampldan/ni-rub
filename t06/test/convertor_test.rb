require 'test/unit'
require_relative '../lib/convertor'

class ConvertorTest < Test::Unit::TestCase
  def prepare_data
    @convertor = Convertor.new
    assert_false @convertor.rates.key? 'NOT_A_CURRENCY'
    @convertor.rates['C01'] = { :base => 1.to_f, :rate => 20.to_f }
    @convertor.rates['C02'] = { :base => 100.to_f, :rate => 5.to_f }
    @convertor.rates['C03'] = { :base => 100.to_f, :rate => 1.to_f }
  end

  def test_convert
    prepare_data
    assert_equal 1.0, @convertor.convert(1.0, 'C01', 'C01')
    assert_equal 320.0, @convertor.convert(0.8, 'C01', 'C02')
    assert_equal 2400.0, @convertor.convert(1.2, 'C01', 'C03')
    assert_equal 0.0025, @convertor.convert(1.0, 'C02', 'C01')
    assert_equal 70.0, @convertor.convert(70.0, 'C02', 'C02')
    assert_equal 0.075, @convertor.convert(0.015, 'C02', 'C03')
    assert_equal 0.0005, @convertor.convert(1.0, 'C03', 'C01')
    assert_equal 0.003, @convertor.convert(0.015, 'C03', 'C02')
    assert_equal 70.0, @convertor.convert(70.0, 'C03', 'C03')
    assert_raise(RuntimeError) { @convertor.convert(1.0, 'NOT_A_CURRENCY', 'C01') }
  end
end
