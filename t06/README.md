# Run without GEM
```
ruby roman.rb <val> <currency> <currency>
ruby roman.rb 10 USD EUR

or

chmod +x ./roman.rb
./roman.rb <val> <currency> <currency>
./roman.rb 10 USD EUR
```

# Run with GEM
```
gem build rates.gemspec
gem install rates-0.1.0.gem

rates <val> <currency> <currency>
rates 10 USD EUR
```

# Run tests
```
ruby -I test ./test/convertor_test.rb
```