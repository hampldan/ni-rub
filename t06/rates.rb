#!/usr/bin/env ruby
require_relative './lib/rates_cli'

app = RatesCli.new
app.run(ARGV)