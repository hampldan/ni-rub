Gem::Specification.new do |s|
  s.name        = 'rates'
  s.version     = '0.1.0'
  s.summary     = "rates"
  s.description = "just rates"
  s.authors     = ["Daniel Hampl"]
  s.email       = 'daniel@hampl.dev'
  s.files       = ["lib/convertor.rb", "lib/rates_cli.rb", "test/convertor_test.rb", "bin/rates"]
  s.homepage    =
    'https://www.youtube.com/watch?v=dQw4w9WgXcQ'
  s.license       = 'MIT'
  s.executables   = ['rates']
  s.add_runtime_dependency 'http'
end