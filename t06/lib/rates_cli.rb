require_relative 'convertor'

class RatesCli
  def initialize
    @convertor = Convertor.new
  end

  # run the program
  def run(argv = [])
    # help
    if argv.length.zero? || (argv.length == 1 && argv[0] == '-h')
      print_help
      return
    end

    # if not propper amount of params then print help wit error
    return error_print_help if argv.length != 3

    # convert
    amount = argv[0].to_f
    source_currency = argv[1]
    target_currency = argv[2]

    converted = @convertor.convert(amount, source_currency, target_currency)

    puts converted
  end

  def print_help
    puts 'Usage: rates.rb [options] <amount> <source_currency> <target_currency>'
    puts 'Options:'
    puts '  -h, --help'
    puts ''
    puts 'Examples:'
    puts '  rates.rb 1 USD EUR'
  end

  def error_print_help
    puts 'ERROR: Invalid input.'
    print_help
  end
end
