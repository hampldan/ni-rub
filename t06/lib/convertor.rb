require "http"

class Convertor
  attr_accessor :rates

  # loads data from CNB server, parses it and stores it in @rates
  def initialize()
    @rates = {}
    response = HTTP.get("https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt")
    body = response.body.to_s
    body.gsub! ',', '.'
    body.split("\n").drop(1).each do |line|
      blocks = line.split("|")
      @rates[blocks[3]] = { :base => blocks[2].to_f, :rate => blocks[4].to_f }
    end
    @rates["CZK"] = { :base => 1, :rate => 1 }
  end

  # converts given amount of money from given currency to given currency
  def convert(amount, input_currency, output_currency)
    if input_currency == output_currency
      return amount
    end

    if @rates[input_currency].nil?
      raise "Unknown currency #{input_currency}"
    end 

    if @rates[output_currency].nil?
      raise "Unknown currency #{output_currency}"
    end

    amount.to_f * @rates[input_currency][:rate] / @rates[output_currency][:rate] / @rates[input_currency][:base] * @rates[output_currency][:base]
  end
end
