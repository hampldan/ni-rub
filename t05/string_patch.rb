require_relative 'roman.rb'


class String
  def to_roman
    Roman.new(self)
  end

  def number
    Roman.new(self).to_i
  end
end
