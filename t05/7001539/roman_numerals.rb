require 'pry'

# ```
#  1  => I
# 10  => X
#  7  => VII
# ```

# There is no need to be able to convert numbers larger than about 3000. (The Romans themselves didn't tend to go any higher)

# Wikipedia says: Modern Roman numerals ... are written by expressing each digit separately starting with the left most digit and skipping any digit with a value of zero.

# To see this in practice, consider the example of 1990.

# In Roman numerals 1990 is MCMXC:

# 1000=M
# 900=CM
# 90=XC

# 2008 is written as MMVIII:

# 2000=MM
# 8=VIII

# See also: http://www.novaroma.org/via_romana/numbers.html

class Integer

  @@values = {
    1=>"I",
    4=>"IV",
    5=>"V",
    9=>"IX",
    10=>"X",
    40=>"XL",
    50=>"L",
    90=>"XC",
    100=>"C",
    400=>"CD",
    500=>"D",
    900=>"CM",
    1000=>"M"
  }

  def to_roman_descending #this one requires numeral-arabic pairs in descending order, i.e. 1000=>"M" on down
    return 0 if self == 0

    roman = ""
    integer = self
    @@values.each do |k,v|
      until integer < k
        roman << v
        integer -= k
      end
    end
    roman
  end

  def to_roman
    integer = self
    roman = ""

    while integer > 0
      if @@values[integer]
        roman += @@values[integer]
        return roman
      end

      roman += @@values[next_lower_key(integer)] # increment the roman numeral string here
      integer -= next_lower_key(integer) # decrement the arabic integer here
    end
  end

  def next_lower_key(integer)
    arabics = @@values.keys
    next_lower_index = (arabics.push(integer).sort.index(integer))-1
    arabics[next_lower_index]
  end
end