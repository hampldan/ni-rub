require_relative 'roman.rb'

class Integer
  def to_roman
    Roman.new(self)
  end

  def roman
    Roman.new(self).to_s
  end
end
