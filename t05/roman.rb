require_relative '7001539/roman_numerals.rb'

class Roman
  include Comparable

  @@ROMAN_TO_INT = {
    "I" => 1,
    "IV" => 4,
    "V" => 5,
    "IX" => 9,
    "X" => 10,
    "XL" => 40,
    "L" => 50,
    "XC" => 90,
    "C" => 100,
    "CD" => 400,
    "D" => 500,
    "CM" => 900,
    "M" => 1000
  }

  # https://stackoverflow.com/questions/53033844/roman-to-integer-refactored
  def roman_to_integer(val)
    number = 0
    str = val.dup
    until str.size.zero?
      last_two_characters = str.slice(-2, 2)
      if @@ROMAN_TO_INT.key?(last_two_characters)
        number += @@ROMAN_TO_INT[last_two_characters]
        str.chop!
      else
        number += @@ROMAN_TO_INT[str.slice(-1)]
      end
      str.chop!
    end
    number
  end

  def initialize(val)
    if val.is_a?(Integer)
      raise 'NOPE' if val < 1 || val > 1000

      @val = val
    elsif val.is_a?(String)
      begin
        @val = roman_to_integer(val)
      rescue TypeError
        raise 'NOPE'
      end
    else
      raise 'NOPE'
    end
  end

  def +(other)
    @val + other
  end

  def -(other)
    @val - other
  end

  def *(other)
    @val * other
  end

  def /(other)
    @val / other
  end

  def coerce(other)
    return [other, self] if other.is_a?(Roman)
    return [Roman.new(other), @val] if other.is_a?(Integer)

    raise 'NOPE'
  end

  def <=>(other)
    @val <=> other
  end

  def to_int
    @val
  end

  def to_i
    to_int
  end

  def to_s
    @val.to_roman
  end

  def succ
    Roman.new(@val + 1)
  end

  def inspect
    to_s
  end
end


require_relative 'int_patch.rb'
require_relative 'string_patch.rb'
