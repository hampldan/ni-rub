require 'json'

class MyStruct
  attr_reader :dict

  def initialize(hash = nil)
    @dict = hash.nil? ? {} : hash.transform_keys(&:to_sym)
  end
  def json_create(object)
    data = JSON.parse(object)
    raise 'Format could not be parsed as hash' if data.class != hash
    @dict = data.transform_keys(&:to_sym)
    self
  end
  def ==(other)
    return false if !other.is_a?(MyStruct)

    @dict == other.dict
  end
  def [](name)
    @dict[name.to_sym]
  end
  def []=(name, val)
    @dict[name.to_sym] = val
  end
  def as_json()
    Json.generate(@dict)
  end
  def delete_field(name)
    key = name.to_sym
    raise "NOPE" if !@dict.key?(key)

    @dict.delete(key)
  end
  def dig(name, *identifiers)
    return @dict[name] if identifiers.empty?

    dig(*identifiers)
  end
  def each_pair
    return @dict.to_enum if !block_given?

    @dict.each_pair { |b| yield b }
    self
  end
  def eql?(other)
    return false if !other.is_a?(MyStruct)
    @dict.eql?(other.dict)
  end
  def freeze
    @dict.freeze
    self
  end
  def frozen?
    return @dict.frozen?
  end
  def inspect
    params = []
    @dict.each_pair{ |key, value| params.append("#{key} => #{value}")}
    "MyStruct{#{params.join(", ")}}"
  end
  def to_h(&block)
    if block_given?
      @dict.to_h(&block)
    else
      @dict.dup
    end
  end
  def to_s
    delf.inspect
  end
  def method_missing(name, *args)
    len = args.length
    setter = name[/.*(?==\z)/m]
    if setter
      raise "NOPE" if len != 1

      self[setter] = args[0]
    else
      raise "NOPE" if len != 0
      self[name]
    end
  end

end
