# frozen_string_literal: true

# Calculator
class Calculator
  # accessors
  attr_writer :name

  # static
  class << self
    def extreme(type, val)
      raise 'Bakka' if val.empty?
      return val.max if type == :max
      return val.min if type == :min

      raise 'Nope'
    end

    def number?(val)
      return true if val.is_a?(Numeric)

      false
    end
  end

  # instance
  def initialize(val = 0)
    @value = val
    @default_valueue = val
    @name = ''
  end

  def add(*val)
    val.each { |i| @value += i }
    self
  end

  def sub(val)
    @value -= val
    self
  end

  def mult(val)
    @value *= val
    self
  end

  def div(val)
    raise 'NiceTry' if val.zero?

    @value /= val
    self
  end

  def result
    @value
  end

  def reset
    @value = @default_valueue
    self
  end

  def name
    @name.upcase
  end
end
